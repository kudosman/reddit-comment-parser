# Reddit Comment Parser #

The Reddit Comment Parser was created to assist with quickly gathering research data from Reddit threads. 

### Why? How? ###

* Some of the threads on reddit (specifically AMA style threads) are ufll of comments that researchers can find useful for some of their projects.   However, they need a quick way to pull the comments and put them into individual text files for analysis.   Normally this meant either they had to do it themselves, or punish an undergrad.  

* Using the PRAW library and some basic python scripting, this script will take a reddit post and dump the individual comments to text files.


### What do I need ###

* Python 2.7+
* PRAW

### How do I use? ###
usage: reddit_parser.py [-h] [--all] [--output OUTPUT_FOLDER] Submission_ID

Scrape a Reddit thread.

positional arguments:

* Submission_ID         The Reddit Submission ID

optional arguments:

* -h, --help            show this help message and exit
* --all                 flag to pull all comments not just top level
* --output OUTPUT_FOLDER
                        folder to output the comment files default is ./output

### Who do I talk to? ###

* Me but I'm probably not going to do much with this