###
# Reddit Comment Parser.  Will take a threadid and pull either top level or all comments and write them out
# to individual text files by comment ID. These text files contain the author and body of comment
###

import os
import praw
import argparse

def pull_all_comments(the_submission_id, pull_all):
    submission = reddit.get_submission(submission_id=the_submission_id)
    submission.replace_more_comments(limit=None, threshold=0)
    #if --all is passed True then flatten the tree.  Else just take the top comments
    if pull_all:
        comments = praw.helpers.flatten_tree(submission.comments)
    else:
        comments = submission.comments
    return comments

def write_comments(comments,output_folder):
    #make folder if necessary
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    #for each comment make a file with the name of the comment ID containing the author and body.
    for x in comments:
        f = open(output_folder + '/' + x.id,'w+')
        f.write(str(x.author)+'\n-----\n')
        f.write(x.body)
        f.close()


def main():
    global the_submission_id
    global reddit

    #Set up arguments
    parser = argparse.ArgumentParser(description='Scrape a Reddit thread.')
    parser.add_argument('the_submission_id', metavar='Submission_ID', type=str, help='The Reddit Submission ID')
    parser.add_argument('--all', dest='all_comments', action='store_true', help='flag to pull all comments not just top level')
    parser.add_argument('--output', dest='output_folder', default='./output/', help='folder to output the comment files default is ./output')
    args = parser.parse_args()

    the_submission_id = args.the_submission_id

    #create Reddit connection
    #should update the user_agent to contain your username as a best practice
    reddit = praw.Reddit(user_agent='Reddit_Comment_Parser')

    comments = pull_all_comments(the_submission_id, args.all_comments)
    write_comments(comments,args.output_folder)

if __name__ == '__main__':
    main()

